package com.example.vp

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.example.vp.databinding.FragmentMainBinding

class MainFragment : Fragment() {
    private var _binding: FragmentMainBinding? = null
    private val binding: FragmentMainBinding get() = _binding!!
    private lateinit var pagerAdapter: GalleryPagerAdapter
    private val imageList = mutableListOf<String>()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentMainBinding.inflate(layoutInflater, container, false)
        init()
        return _binding!!.root
    }

    private fun init() {
        if (imageList.isEmpty()) {
            setData()
        }
        val onImageClickListener = object :OnImageClickListener{
            override fun onImageClick(position: Int) {
                openFullScreenImage(position)
            }

        }
        pagerAdapter = GalleryPagerAdapter(requireContext(), imageList,onImageClickListener)
        binding.vpGallery.adapter = pagerAdapter
    }
    private fun openFullScreenImage(position:Int){
        val action = MainFragmentDirections.actionMainFragmentToImageFragment(imageList[position])
        Navigation.findNavController(binding.root).navigate(action)
    }

    private fun setData() {
        imageList.apply {
            add("https://paininthearsenal.com/wp-content/uploads/getty-images/2017/07/1255767977.jpeg")
            add("https://www.arsenal.com/sites/default/files/styles/player_featured_image_1045x658/public/images/Saka_1045x658_0.jpg?itok=Tx1tc5do")
            add("https://pbs.twimg.com/media/EuNarr7XAAIWU60.jpg")
            add("https://goonertalk.com/wp-content/uploads/2020/05/0_Premier-League-Arsenal-v-Aston-Villa.jpg")
            add("https://d3vlf99qeg6bpx.cloudfront.net/content/uploads/2021/04/15160840/Gabriel-Magalhaes-Arsenal-FC-TEAMtalk.jpg")
            add("https://images.daznservices.com/di/library/GOAL/43/ea/alexandre-lacazette-arsenal-2019-20_fvy05fqyhivm10oypxv1npaht.jpg?t=-1622901884&quality=60&w=1200&h=800")
            add("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTC1OpoX-lvpdyGSm1fyHvPlrUaRczyoMx9MA&usqp=CAU")
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}