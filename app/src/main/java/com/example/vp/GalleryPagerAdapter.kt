package com.example.vp

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.viewpager.widget.PagerAdapter
import com.bumptech.glide.Glide

class GalleryPagerAdapter(
    val context: Context,
    val imageList: MutableList<String>,
    val onImageClickListener: OnImageClickListener
) : PagerAdapter() {
    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val view: View = LayoutInflater.from(context).inflate(R.layout.row_image, container, false)
        val imageView = view.findViewById<View>(R.id.ivGalleryImage) as ImageView
        Glide.with(context).load(imageList[position]).into(imageView)

        imageView.setOnClickListener {
            onImageClickListener.onImageClick(position)
        }
        container.addView(view)
        return view
    }

    override fun getCount(): Int = imageList.size

    override fun isViewFromObject(view: View, o: Any): Boolean {
        return view === o
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }

    override fun getItemPosition(`object`: Any): Int {
        return super.getItemPosition(`object`)
    }
}